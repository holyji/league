import grade_regression
import grade_score
import pandas as pd
import requests
import time
import math
import sys


# sys.argv[1] = summoner id
# sys.argv[2] = api key

summoner_name = requests.get('https://na1.api.riotgames.com/lol/summoner/v3/summoners/by-account/' + sys.argv[1] + '?api_key=' + sys.argv[2]).json()['name']
match_list = []

# get matchlist
summoner_req = requests.get('https://na1.api.riotgames.com/lol/match/v3/matchlists/by-account/' + sys.argv[1] + '?api_key=' + sys.argv[2])

loop = 0
# get data from each match in matchlist
for match in summoner_req.json()['matches']:
	if loop > 19: 
		break

	match_req = requests.get('https://na1.api.riotgames.com/lol/match/v3/matches/' + str(match['gameId']) + '?api_key=' + sys.argv[2])
	if 'participantIdentities' in match_req.json():
		duration = match_req.json()['gameDuration']
		# get participantId
		for player in match_req.json()['participantIdentities']:
			if player['player']['summonerName'] == summoner_name:
				participant_id = player['participantId']

		# get stats and save in list
		for player in match_req.json()['participants']:
			if player['participantId'] == participant_id:
				s = {'KDA':(float(player['stats']['kills']+player['stats']['assists'])/player['stats']['deaths'])/(duration/60.0), 
				'CS':player['stats']['totalMinionsKilled']/(duration/60.0), 
				'TD':player['stats']['totalDamageDealt']/(duration/60.0)}
				match_list.append(s)

	# delay each iter to dodge rate limiter
	time.sleep(.2)
	loop += 1

X = pd.DataFrame(data=match_list)
predictions = grade_regression.lm.predict(X)
for score in predictions:
	if int(score) in grade_score.score_convert:
		if math.ceil(score) - score <= .5 and math.ceil(score)<=12:
			print score, ' => ', grade_score.score_convert[int(score)], '/', grade_score.score_convert[int(score)+1]
		else:
			print score, ' => ', grade_score.score_convert[int(score)]
	else:
		print score, ' => ', 'D'