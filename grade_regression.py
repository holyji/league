import pandas as pd
#import get_grades
#import get_matches
import sqlite3
import ast
from sklearn import linear_model

conn = sqlite3.connect('league.db')
c = conn.cursor()

# graded games per hero and lane in ranked queue
c.execute('SELECT * FROM Mastery WHERE _rowid_ IN \
	(SELECT _rowid_ FROM Game WHERE (role="SOLO" OR role="DUO") \
	AND  champion=103 AND lane="MID") AND \
	Grade!="0"')
grade_list = list(c.fetchall())
c.execute('SELECT json, duration FROM Game WHERE _rowid_ IN \
	(SELECT _rowid_ FROM Mastery WHERE _rowid_ IN \
	(SELECT _rowid_ FROM Game WHERE (role="SOLO" OR role="DUO") \
	AND  champion=103 AND lane="MID") AND \
	Grade!="0")')
player_stats = list(c.fetchall())
stats_list = []
stats_list_time = []

# extract relevant game stats to evaluate as dependent variables
for stats in player_stats:
	stats_eval = ast.literal_eval(stats[0])
	stats_dict = {}
	stats_dict['KDA'] = float(stats_eval['kills']+stats_eval['assists'])/stats_eval['deaths']
	stats_dict['CS'] = stats_eval['totalMinionsKilled']
	stats_dict['TD'] = stats_eval['totalDamageDealt']
	stats_list.append(stats_dict)
	stats_list_time.append({'KDA':stats_dict['KDA']/(stats[1]/60.0),
							'CS':stats_dict['CS']/(stats[1]/60.0),
							'TD':stats_dict['TD']/(stats[1]/60.0)})

df = pd.DataFrame(data=stats_list)
df_time = pd.DataFrame(data=stats_list_time)
target = pd.DataFrame(data=grade_list, columns=['Grade'])

lm = linear_model.LinearRegression()
model_time = lm.fit(df_time,target)
print "R^2: ", lm.score(df_time,target)

# coef = lm.coef_
# intercept = lm.intercept_

conn.commit()
conn.close()