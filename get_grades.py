from bs4 import BeautifulSoup
import sqlite3
import grade_score


conn = sqlite3.connect('league.db')
c = conn.cursor()
c.execute('''CREATE TABLE IF NOT EXISTS Mastery
			(grade text)''')

# scrape grades from cached match history webpage
with open('Desktop/MatchHistory.html', 'rb') as html_doc:
	soup = BeautifulSoup(html_doc.read(), 'html.parser')
	for grade in soup.find_all('span', class_='game-delta-delta'):
		if grade.text[:2] in grade_score.grade_convert:
			c.execute("INSERT INTO Mastery VALUES(?)", (grade_score.grade_convert[grade.text[:2]],))
		else:
			c.execute("INSERT INTO Mastery VALUES(?)", ('0',))

html_doc.close()
conn.commit()
conn.close()