import requests
import time
import sqlite3

conn = sqlite3.connect('league.db')
c = conn.cursor()
c.execute('''CREATE TABLE IF NOT EXISTS Game
			(role text, champion integer, lane text, duration integer, json text)''')

# expired :)
api_key = 'RGAPI-27c7d25c-6b4d-4d80-9dbc-6ddaf95bba12'

# get matchlist
summoner_req = requests.get('https://na1.api.riotgames.com/lol/match/v3/matchlists/by-account/206048482?api_key=' + api_key)

# get data from each match in matchlist
for match in summoner_req.json()['matches']:
	role = match['role']
	champion = match['champion']
	lane = match['lane']
	match_req = requests.get('https://na1.api.riotgames.com/lol/match/v3/matches/' + str(match['gameId']) + '?api_key=' + api_key)
	if 'participantIdentities' in match_req.json():
		duration = match_req.json()['gameDuration']
		# get participantId
		for player in match_req.json()['participantIdentities']:
			if player['player']['summonerName'] == 'scribblesMD':
				participant_id = player['participantId']

		# get stats and save all data in db
		for player in match_req.json()['participants']:
			if player['participantId'] == participant_id:
				c.execute('INSERT INTO Game VALUES(?,?,?,?,?)', 
					(role, champion, lane, duration, str(player['stats'])))

	# delay each iter to dodge rate limiter
	time.sleep(.2)


conn.commit()
conn.close()