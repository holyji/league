# dictionary of grades as int values
grade_convert = {
	'S+' : '12', 'S ' : '11', 'S-' : '10',
	'A+' : '9', 'A ' : '8', 'A-' : '7',
	'B+' : '6', 'B ' : '5', 'B-' : '4',
	'C+' : '3', 'C ' : '2', 'C-' : '1'
}

# dictionary of int as grades
score_convert = {
	12 : 'S+', 11 : 'S ', 10 : 'S-',
	9 : 'A+', 8 : 'A ', 7 : 'A-',
	6 : 'B+', 5 : 'B ', 4 : 'B-',
	3 : 'C+', 2 : 'C ', 1 : 'C-'
}